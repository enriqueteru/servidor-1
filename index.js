//EXPRESS
const express = require("express");
const session= require("express-session");
const PORT = 3000;
const server = express();
const path = require("path");
const bodyParser = require("body-parser");

const passport = require('passport');
require('./authentication/passport'); // Requerimos nuestro archivo de configuración



//Sesiones de express 

server.use(
  session({
    secret: 'upgradehub_node', // ¡Este secreto tendremos que cambiarlo en producción!
    resave: false, // Solo guardará la sesión si hay cambios en ella.
    saveUninitialized: false, // Lo usaremos como false debido a que gestionamos nuestra sesión con Passport
    cookie: {
      maxAge: 3600000 // Milisegundos de duración de nuestra cookie, en este caso será una hora.
    },
  })
);

//Midllewares de PASSPORT
server.use(passport.initialize());
server.use(passport.session());


//CONNECT TO MONGODB
require("./utils/db");

server.use(express.json());
server.use(express.urlencoded({ extended: true }));

//STATIC
server.use(express.static(path.join(__dirname, "public")));




//ROUTES
/*PORTFOLIO*/
const portfolioRoutes = require("./routes/portfolio.routes");
server.use("/portfolio", portfolioRoutes);

/*SEARCH*/
const searchRoutes = require("./routes/search.routes");
server.use("/search", searchRoutes);

/*DELETE*/
const deleteRoutes = require("./routes/delete.routes");
server.use("/delete", deleteRoutes);

/*PUT*/
const putRoutes = require("./routes/put.routes");
server.use("/edit", putRoutes);

/*LOCATIONS*/
const categoriesRoutes = require("./routes/categorie.routes");
server.use("/categories", categoriesRoutes);

/*USERS*/
const userRoutes = require("./routes/user.routes");
server.use("/users", userRoutes);

// PASSPORT USE
server.use(passport.initialize());

/*HOME*/
const indexRoutes = require("./routes/index.routes");
server.use("/", indexRoutes);


/*CONTROL DE ERRORES*/
server.use("*", (req, res, next) => {
  const error = new Error("Route not found");
  error.status = 404;
  next(error);
});

server.use((error, req, res) => {
    const status = status(error.status || 500)
    const message =  json(error.message || "Unexpected error");
	return res.status(status).json(message);
});

//LISTEN TO SERVER
server.listen(PORT, () => {
  console.log(`running http://localhost:${PORT}`);
});

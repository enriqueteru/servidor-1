const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Portfolio = require("../models/Portfolio");

const CategorieSchema = new Schema({ 
name: { type : String, require: true},
portfolio: [{type: mongoose.Types.ObjectId, ref: 'Portfolio'}]
})

const Categorie = mongoose.model('Categorie', CategorieSchema);

module.exports = Categorie;
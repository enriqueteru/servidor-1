const mongoose = require('mongoose');

//Schema 
const Schema = mongoose.Schema;
const PortfolioSchema = new Schema({
proyect: String,
software: Array,
year: Number,
skills: Array,
description: String,
images: Array,
});
//Model
const Portfolio = mongoose.model('Portfolio', PortfolioSchema );
module.exports = Portfolio;



fetch("http://localhost:3000/portfolio")
  .then((response) => response.json())
  .then((data) => {
    console.log(data);

    const result = document.querySelector("#result");
    let count = -1;

    data.forEach((element) => {
      count++;
      const divD = document.createElement("div");
      const buttonD = document.createElement("button");
      const img = document.createElement("img");
      const title = document.createElement("h3");
      divD.setAttribute("class", "divD");
      buttonD.textContent = `X`;
      buttonD.setAttribute("class", "buttonD");
      title.textContent = element.proyect;
      title.setAttribute("class", "titleD");
      img.src = element.images[0];
      img.id = "i" + count;
      img.setAttribute("class", "imgD");
      img.setAttribute("id", count + "cover");
      result.appendChild(divD);
      divD.appendChild(img);
      divD.appendChild(buttonD);
      divD.appendChild(title);

      buttonD.addEventListener("click", () => {
        fetch(`http://localhost:3000/delete/name/${element.proyect}`, {
          method: "DELETE",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
          },
        })
          .then(alert(`${element.proyect} Deleted, recargando DB`))
          .finally(
            setInterval(() => {
              window.location = `http://localhost:3000/delete/`;
            }, 500)
          );
      });
    });
  })

  .finally(() => {
    console.log("Promise finish");
  });

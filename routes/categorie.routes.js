const express = require("express");
const mongoose = require("mongoose");
const router = express.Router();
const Categorie = require("../models/Categorie");




/*router.get("/", (req, res, next) => {
  try {
    res.send("<h1>Categories</h1>");
  } catch (error) {
    next(error);
  }
});
*/


//GET

router.get("/",async (req, res, next) => {
  try {
const categorie = await Categorie.find().populate(
'portfolio', 'proyect year images description').lean();
return res.status(200).json(categorie);

  } catch (error) {
    next(error);
  }
});

//POST

router.post("/new", async (req, res, next) => {
  try {
    const newCategorie = new Categorie({
      name: req.body.name,
      portfolios: [],
    });

    const saveCategorie = await newCategorie.save();

    res.status(202).json(saveCategorie);
  } catch (error) {
    next(error);
  }
});

//PUT 
router.put("/add-proyect-to-cat", async (req, res, next) => {
  try {
    const { categorieId } = req.body;
    const { portfolioId } = req.body;

    const updateCategorie = await Categorie.findByIdAndUpdate(
      categorieId,
      { $addToSet: { portfolio: portfolioId } },
      { new: true }
    );

    return res.status(202).json(updateCategorie);
  } catch (error) {
    next(error);
  }
});

module.exports = router;

const express = require("express");
const Portfolio = require("../models/Portfolio");
const router = express.Router();
const path = require("path");

router.get("/", (req, res, next) => {
  try {
    res.sendFile(path.resolve("./view/delete.html"));
  } catch (error) {
    next(error);
  }
});

/*DELETE by name key INSENSITIVE*/
router.delete("/name/:name", async (req, res, next) => {
  try {
    const { name } = req.params;

    result = await Portfolio.findOneAndDelete({
      proyect: { $regex: new RegExp("^" + name.toLowerCase(), "i") },
    });
    return res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

module.exports = router;

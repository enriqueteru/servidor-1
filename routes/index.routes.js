const express = require("express");
const Portfolio = require("../models/Portfolio");
const router = express.Router();
const path = require("path");

router.get("/", (req, res, next) => {
  try {
    res.sendFile(path.resolve("./view/home.html"));
  } catch (error) {
    next(error);
  }
});


module.exports = router;

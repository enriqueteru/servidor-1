const express = require("express");
const Portfolio = require("../models/Portfolio");
const router = express.Router();
const path = require("path");
const bodyParser = require("body-parser");

router.use(bodyParser.urlencoded({ extended: true }));

//GETS

// All Proyects
router.get("/", async (req, res) => {
  try {
    const portfolios = await Portfolio.find();
    return res.status(200).json(portfolios);
  } catch (error) {
    return next(error)
  }
});

/*     //Show by ID
router.get("/id/:id", async (req, res) => {
  const id = req.params.id;
  try {
    const portfolios = await Portfolio.findById(id);
    if (portfolios) {
      return res.status(200).json(portfolios);
    } else {
      return res.status(404).json("No id found by this id");
    }
  } catch (err) {
    return res.status(500).json(err);
  }   return next(error)
}); */

//Show by Proyect NAME
router.get("/proyect/:proyect", async (req, res, next) => {
  const {proyect} = req.params;
  try {
    const proyectByName = await Portfolio.find({
      proyect: { $regex: new RegExp("^" + proyect.toLowerCase(), "i") },
    });
    return res.status(200).json(proyectByName);
  } catch (error) {
    return next(error)
 
  }
});

//Show by YEAR GREATER OR SAME THAN X
router.get("/year/:year", async (req, res, next) => {
  const {year} = req.params;

  try {
    const proyectByYear = await Portfolio.find({ year: { $gte: year } });
    return res.status(200).json(proyectByYear);
  } catch (error) {
    return next(error);
  }
});

//Show by Software USED
router.get("/software/:software", async (req, res, next) => {
  const software = req.params.software;
  try {
    const bySoftware = await Portfolio.find({
      software: { $regex: new RegExp("^" + software.toLowerCase(), "i") },
    });
    return res.status(200).json(bySoftware);
  } catch (error) {
    return next(error);
  }
});

//Show by skills USED
router.get("/skill/:skills", async (req, res, next) => {
  const {skills}= req.params;
  try {
    const bySKills = await Portfolio.find({
      skills: { $regex: new RegExp("^" + skills.toLowerCase(), "i") },
    });
    return res.status(200).json(bySKills);
  } catch (error) {
    return next(error);
  }
});

//POST
router.get("/new", (req, res, next) => {

  try{
  res.sendFile(path.resolve("./view/new-proyect.html"));}
  catch(error){
  return next(error);
  }
});

//Create a new Proyect
router.post("/new", async (req, res, next) => {
  try {
    //creating new poryect
    const newPortfolio = new Portfolio({
      proyect: req.body.proyect,
      software: req.body.software.replace(/ +/g, " ").split(","),
      year: req.body.year,
      skills: req.body.skills.replace(/ +/g, " ").split(","),
      description: req.body.description,
      images: req.body.images.replace(/ +/g, " ").split(","),
    });
    const createdPortfolio = await newPortfolio.save();
    console.log(createdPortfolio);
    return res.redirect(
      "/portfolio/new"
    ) /* ESTO MOSTRARÍA EL RESULTADO // res.status(201).json(createdPortfolio) */;
  } catch (error) {
   return next(error);
  }
});








module.exports = router;

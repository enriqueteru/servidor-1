const express = require("express");
const router = express.Router();
const path = require("path");
const Portfolio = require("../models/Portfolio");

router.get("/", (req, res, next) => {
  try {
    res.sendFile(path.resolve("./view/edit.html"));
  } catch (error) {
    return next(error);
  }
});

router.put("/id/:id", async (req, res, next) => {
  try {
const {id} = req.params;
const ProyectModification = new Portfolio(req.body);
ProyectModification._id = id;
const ProyectUpdate = await Portfolio.findByIdAndUpdate(id, ProyectModification);
const result = await Portfolio.findById(id);
    return res.status(200).json(ProyectUpdate);
  } catch (error) {
    next(error);
  }
});

module.exports = router;



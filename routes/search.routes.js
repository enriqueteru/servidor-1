const express = require("express");
const router = express.Router();
const path = require("path");

router.get("/", (req, res, next) => {
  try {
    res.sendFile(path.resolve("./view/search.html"));
  } catch (error) {
    next(error);
  }
});

module.exports = router;

const express = require("express");
const router = express.Router();
const passport = require("passport");
const Users = require("../models/User");

router.get("/", async (req, res, next) => {
  try {
    const users = await Users.find();
    res.status(202).json(users);
  } catch (error) {
    next(error);
  }
});

router.post("/register", (req, res, next) => {
  passport.authenticate("register", (error, user) => {
    if (error) {
      return next(error);
    }


    req.logIn(user, (error) => {
      // Si hay un error logeando al usuario, resolvemos el controlador
      if (error) {
          return next(error);
        }})

    return res.status(201).json(user);
  })(req);
});



router.post('/login', (req, res, next) => {
  passport.authenticate('login', (error, user) => {
      if (error) return next(error)

      req.logIn(user, (error) => {
          // Si hay un error logeando al usuario, resolvemos el controlador
          if (error) {
              return next(error);
          }
          // Si no hay error, devolvemos al usuario logueado
          return res.status(200).json(user)
      });
  })(req);
});

module.exports = router;

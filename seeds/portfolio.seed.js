const mongoose = require("mongoose");

// Imporatmos el modelo Pet en este nuevo archivo.
const Portfolio = require("../models/Portfolio");

const portfoliop = [
  {
    proyect: "Carcelen",
    software: ["Wordpress", "JS", "CSS", "SASS", "MyQSL", "AI", "PS"],
    year: 2021,
    skills: ["Diseño web", "diseño gráfico", "Querys"],
    description:
      "Carcelén es más que una agencia de talento. Es una marca. Y una empresa multidisciplinar líder en España de representación de artistas, se ha convertido con los años en un hub capaz de aglutinar actores, directores, productores, guionistas y autores.",
    images: [
      "https://enriqueteruel.com/wp-content/uploads/2021/11/mockup-imac-copia-2000x1500-1.jpg",
      "https://enriqueteruel.com/wp-content/uploads/2021/11/01-business-cards2-copia-2000x1335-1.jpg",
      "https://enriqueteruel.com/wp-content/uploads/2021/11/carcelen-branding-web-bbrand-05.jpg",
    ],
  },

  {
    proyect: "Headon",
    software: ["Zbrush", "C4D", "AI", "PS", "Octane Render"],
    year: 2020,
    skills: ["Diseño web", "diseño gráfico", "Modelado 3D", "Packaging"],
    description:
      "HEADON es una banda de metal alternativo, con un sonido potente y duro, guitarras contundentes y voces muy melódicas.",
    images: [
      "https://enriqueteruel.com/wp-content/uploads/2021/04/Genesis-packagin-2-scaled.jpg",
      "https://enriqueteruel.com/wp-content/uploads/2021/04/Revolucion-portada-genesis-scaled.jpg",
      "https://enriqueteruel.com/wp-content/uploads/2021/04/headon-asphyxia-portada-scaled.jpg",
      "https://enriqueteruel.com/wp-content/uploads/2021/04/camiseta-headon-asphyxia-genesis-scaled.jpg",
    ],
  },
  {
    proyect: "Pio",
    software: ["Wordpress", "JS", "CSS", "SASS", "MyQSL"],
    year: 2019,

    skills: [
      "Diseño web",
      "Diseño Gráfico",
      "E-commercer",
      "Woo-commercer",
      "Stock",
      "Packaging",
    ],
    description:
      "Entre las sierras del madroño y parda, se levantan las bodegas Pío del Ramo, fundadas en 2007 gracias al empeño y valentía de su creador: Pío del Ramo Nuñez.",
    images: [
      "https://enriqueteruel.com/wp-content/uploads/2021/11/pio-packaging-bbrand-02.jpg",
      "https://enriqueteruel.com/wp-content/uploads/2021/11/pio-packaging-bbrand-07.jpg",
    ],
  },

  {
    proyect: "OISS",
    software: ["Wordpress", "JS", "CSS", "SASS", "MyQSL"],
    year: 2015,
    skills: ["Diseño web", "Diseño Gráfico", "Marketing"],
    description:
      "La Organización Iberoamericana de Seguridad Social, también conocida por su acrónimo OISS, es un organismo intergubernamental que trata de promover el bienestar económico y social de los países que integran la Comunidad Iberoamericana mediante el intercambio de experiencias relacionadas con la Seguridad Social.",
    images: [
      "https://enriqueteruel.com/wp-content/uploads/2021/04/Oiss-branding-papeleria.jpg",
      "https://enriqueteruel.com/wp-content/uploads/2021/04/Oiss-branding-redes-sociales-1.jpg",
      "https://enriqueteruel.com/wp-content/uploads/2021/04/Oiss-branding-digital-2.jpg",
      "https://enriqueteruel.com/wp-content/uploads/2021/04/Oiss-branding-digital-1.jpg",
    ],
  },

  {
    proyect: "Casablanca",
    software: ["Wordpress", "Moodle", "CSS", "SASS", "MyQSL"],
    year: 2015,
    skills: ["Diseño web", "Gestión de Alumnos", "Diseño Gráfico", "Formación"],
    description:
      "Colegio Casablanca es una institución educativa que se preocupa por el ser humano, lo forma en valores y lo proyecta hacia la comunidad para que le ayude a un mejor futuro.  La visión de la educación nos inspira hoy.​",
    images: [
      "https://enriqueteruel.com/wp-content/uploads/2021/04/colcasablanca-web-colegio-colombia-red-2.jpg",
      "https://enriqueteruel.com/wp-content/uploads/2021/04/colcasablanca-web-colegio-colombia-red-6.jpg",
      "https://enriqueteruel.com/wp-content/uploads/2021/04/colcasablanca-web-colegio-colombia-red-3.jpg",
      "https://enriqueteruel.com/wp-content/uploads/2021/04/colcasablanca-web-colegio-colombia-red-4.jpg",
    ],
  },

  {
    proyect: "ISIO",
    software: ["Wordpress", "Pixel-perfect", "CSS", "SASS", "MyQSL", "PHP"],
    year: 20201,
    skills: ["Diseño web", "Diseño Gráfico", "Formación"],
    description:
      "Winemaking adventure, colaborador de proyectos vinícolas integrales. Donde todos cuentan​",
    images: [
      "https://isio.enriqueteruel.com/wp-content/uploads/2021/06/isio-ramos-1-scaled.jpg",
      "https://isio.enriqueteruel.com/wp-content/uploads/2021/06/isio-ramos-2-scaled.jpg",
      "https://isio.enriqueteruel.com/wp-content/uploads/2021/06/isio-ramos-5-scaled.jpgg",
      "https://isio.enriqueteruel.com/wp-content/uploads/2021/06/ISIO-30-scaled.jpg",
    ],
  },
  {
    proyect: "HJL",
    software: ["Wordpress", "Pixel-perfect", "CSS", "SASS", "MyQSL", "PHP"],
    year: 2015,
    skills: ["Diseño web", "Diseño Gráfico", "Formación"],
    description:
      "Somos una empresa familiar con tradición en el mundo del metal dedicada al diseño, producción y comercialización de productos metálicos cortados, troquelados y estampados.",
    images: [
      "https://hjl.es/wp-content/uploads/2021/09/Recurso-44HJL.png",
      "https://hjl.es/wp-content/uploads/2021/09/Recurso-40HJL.png",
      "https://hjl.es/wp-content/uploads/2021/09/Recurso-9HJL.png",
      "https://hjl.es/wp-content/uploads/2021/05/Recurso-4.svg",
    ],
  },
  {
    proyect: "OSTARA",
    software: ["Wordpress", "Pixel-perfect", "CSS", "SASS", "MyQSL", "PHP"],
    year: 2020,
    skills: ["Diseño web", "Diseño Gráfico", "Formación"],
    description:
      "OSTARA es un espacio donde la comida para llevar se fusiona con la alta cocina de autor en el centro de Murcia.",
    images: [
      "https://ostara.es/wp-content/uploads/2021/07/Recurso-1.jpg",
      "https://ostara.es/wp-content/uploads/2021/07/lidye-1Shk_PkNkNw-unsplash-min-scaled.jpg",
      "https://ostara.es/wp-content/uploads/2021/07/dan-gold-4_jhDO54BYg-unsplash-min-scaled.jpg",
      "https://ostara.es/wp-content/uploads/2021/09/ella-olsson-lMcRyBx4G50-unsplash-min-scaled.jpg",
    ],
  },
  {
    proyect: "David Compositor",
    software: ["Wordpress", "Pixel-perfect", "CSS", "SASS", "MyQSL", "PHP"],
    year: 2021,
    skills: ["Diseño web", "Diseño Gráfico", "Formación"],
    description:
      "buscar, crear y recrear mundos sonoros únicos que nos transporten hasta el momento de escucha deseado para cada proyecto.",
    images: [
      "https://enriqueteruel.com/wp-content/uploads/2021/11/p1-5.jpg",
      "https://enriqueteruel.com/wp-content/uploads/2021/11/p1-6-scaled.jpg",
      "https://enriqueteruel.com/wp-content/uploads/2021/11/p1-4-scaled.jpg",
    ],
  },
  {
    proyect: "Quota",
    software: ["WordPress", " PS", " AI"],
    year: 2013,
    skills: ["Marketing", " Diseño Gráfico"],
    description:
      " Asesores de confianza Quota Asesores y Consultores,S.L. es una empresa fundada en 1996 por Pedro Mazeres Gaitero, Manuel Fernández-Cuartero Luque y Ricardo Teruel Sánchez. Con sede en San Javier hemos sabido consolidar y mantener, hasta el día de hoy, una posición de primera línea en el ámbito del asesoramiento empresarial, avalados por las competencias reconocidas.",
    images: [
      "https://enriqueteruel.com/wp-content/uploads/2019/11/quota_-06.jpg",
      " https://enriqueteruel.com/wp-content/uploads/2019/11/quotareducido-05.jpg",
      " https://enriqueteruel.com/wp-content/uploads/2019/11/quotareducido-04.jpg",
      " https://enriqueteruel.com/wp-content/uploads/2019/11/quota_-01.jpg",
    ],
  },
  {
    proyect: "Cinesol",
    software: ["WordPress", " PS", " AI", "PHP"],
    year: 2015,
    skills: ["Marketing", "Pixel-perfect", " Diseño Gráfico"],
    description:
      "Soluciones en equipo y servicios técnicos para profesionales del sonido en cine y audiovisual.",
    images: [
      "https://images.unsplash.com/photo-1485846234645-a62644f84728?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1459&q=80",
      "https://images.unsplash.com/photo-1497015289639-54688650d173?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1632&q=80",
      "https://images.unsplash.com/photo-1493804714600-6edb1cd93080?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
      "https://images.unsplash.com/photo-1535540878298-a155c6d065ef?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
    ],
  },
  {
    proyect: "Santa Rosalía Resort",
    software: ["WordPress", " PS", " AI", "PHP"],
    year: 2015,
    skills: ["Marketing", "Pixel-perfect", " Diseño Gráfico"],
    description:
      "Santa Rosalía, un Resort en Murcia en el que se encontrará la segunda laguna Crystal Lagoons® de España, unas aguas cristalinas semejantes a un paraje tropical",
    images: [
      "https://enriqueteruel.com/wp-content/uploads/2021/04/santa-rosalia-Lake-and-Life-Resort-1.jpg",
      "https://enriqueteruel.com/wp-content/uploads/2021/04/santa-rosalia-Lake-and-Life-Resort-3.jpg",
      "https://enriqueteruel.com/wp-content/uploads/2021/04/santa-rosalia-Lake-and-Life-Resort-9.jpg",
      "https://enriqueteruel.com/wp-content/uploads/2021/04/santa-rpsalia-1.jpg",
    ],
  },
  {
    proyect: "Evohe Resorts",
    software: ["WordPress", " PS", " AI", "PHP"],
    year: 2015,
    skills: ["Marketing", "Pixel-perfect", " Diseño Gráfico"],
    description:
      "Evohe ofrece una selección de viviendas de nueva construcción y reventa en las zonas mas atractivas y los mejores resorts de Murcia y el sur de Alicante.",
    images: [
      "https://enriqueteruel.com/wp-content/uploads/2019/11/Untitled-1-05-1.jpg",
      "https://enriqueteruel.com/wp-content/uploads/2019/11/Untitled-1-03.jpg",
      "https://enriqueteruel.com/wp-content/uploads/2019/11/Untitled-1-04-1.jpg",
      "https://enriqueteruel.com/wp-content/uploads/2019/11/Untitled-1-06.jpg",
    ],
  },
];

const portfolioDocuments = portfoliop.map((item) => new Portfolio(item));

mongoose
  .connect(
    "mongodb+srv://EnriqueT:f8suQHCWc5vZbrGi@cluster0.1agt3.mongodb.net/Cluster0?retryWrites=true&w=majority",
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }
  )
  .then(async () => {
    // Utilizando Character.find() obtendremos un array con todos los personajes de la db
    const allPortfolio = await Portfolio.find();

    // Si existen personajes previamente, dropearemos la colección
    if (allPortfolio.length) {
      await Portfolio.collection.drop(); //La función drop borra la colección
    }
  })
  .catch((err) => console.log(`Error deleting data: ${err}`))
  .then(async () => {
    // Una vez vaciada la db de los personajes, usaremos el array characterDocuments
    // para llenar nuestra base de datos con todas los personajes.
    await Portfolio.insertMany(portfolioDocuments);
  })
  .then(async () => console.log("DB updated!!!"))
  .catch((err) => console.log(`Error creating data: ${err}`))
  // Por último nos desconectaremos de la DB.
  .finally(() => mongoose.disconnect());
